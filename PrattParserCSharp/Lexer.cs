﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;

namespace General.PrattParser
{
    public class Lexer : IEnumerator<Token>
    {
        private readonly Dictionary<char, TokenType> _punctuators;
        private readonly string _text;
        private int _index;

        public Token Current { get; private set; }

        object IEnumerator.Current => Current;

        public Lexer(string text)
        {
            _text = text;
            _punctuators = new Dictionary<char, TokenType>();
            // Register all of the TokenTypes that are explicit punctuators.
            foreach (var tokenType in (TokenType[])Enum.GetValues(typeof(TokenType)))
            {
                var punctuator = tokenType.Punctuator();
                if (punctuator != default(char))
                    _punctuators[punctuator] = tokenType;
            }
        }

        public bool MoveNext()
        {
            while (_index < _text.Length)
            {
                var c = _text[_index++];
                if (_punctuators.ContainsKey(c))
                {
                    // Handle punctuation.
                    Current = new Token(_punctuators[c], c.ToString(CultureInfo.InvariantCulture));
                    return true;
                }
                if (!char.IsLetterOrDigit(c)) continue;
                // Handle names.
                var start = _index - 1;
                while (_index < _text.Length)
                {
                    if (!char.IsLetter(_text[_index])) break;
                    _index++;
                }
                var name = _text.Substring(start, _index - start);
                Current = new Token(TokenType.Name, name);
                return true;
                // Else ignore all other characters (whitespace, etc.)
            }
            // Once we've reached the end of the string, just return EOF tokens. We'll
            // just keeping returning them as many times as we're asked so that the
            // parser's lookahead doesn't have to worry about running out of tokens.
            Current = new Token(TokenType.Eof, "");
            return true;
        }

        public void Reset() { _index = 0; }

        public void Dispose() { }
    }
}
