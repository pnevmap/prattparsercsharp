﻿using System.Collections.Generic;

namespace General.PrattParser
{
    public class PrattParser<TExpression>
        where TExpression : IExpression
    {
        private readonly IEnumerator<Token> _tokens;
        private readonly List<Token> _read = new List<Token>();
        private readonly Dictionary<TokenType, IPrefixParselet<TExpression>> _prefixParselets = new Dictionary<TokenType, IPrefixParselet<TExpression>>();
        private readonly Dictionary<TokenType, IInfixParselet<TExpression>> _infixParselets = new Dictionary<TokenType, IInfixParselet<TExpression>>();

        protected PrattParser(IEnumerator<Token> tokens)
        {
            _tokens = tokens;
        }

        public TExpression ParseExpression(out string error, int precedence = 0)
        {
            var token = Consume();
            var prefix = _prefixParselets[token.Type];
            if (prefix == null)
            {
                error = "Could not parse \"" + token.Text + "\".";
                return default(TExpression);
            }
            var left = prefix.Parse(out error, this, token);
            if (error != string.Empty) return default(TExpression);
            while (precedence < GetPrecedence())
            {
                token = Consume();
                var infix = _infixParselets[token.Type];
                left = infix.Parse(out error, this, left, token);
                if (error != string.Empty) return default(TExpression);
            }
            return left;
        }

        public bool Match(TokenType expected)
        {
            var token = LookAhead(0);
            if (token.Type != expected) return false;
            Consume();
            return true;
        }

        public Token Consume(out string error, TokenType expected)
        {
            error = string.Empty;
            var token = LookAhead(0);
            if (token.Type == expected) return Consume();
            error = "Expected token " + expected + " and found " + token.Type;
            return null;
        }

        private Token Consume()
        {
            // Make sure we've read the token.
            LookAhead(0);
            var token = _read[0];
            _read.RemoveAt(0);
            return token;
        }

        private Token LookAhead(int distance)
        {
            // Read in as many as needed.
            while (distance >= _read.Count)
            {
                _tokens.MoveNext();
                _read.Add(_tokens.Current);
            }
            // Get the queued token.
            return _read[distance];
        }

        private int GetPrecedence()
        {
            IInfixParselet<TExpression> parser;
            _infixParselets.TryGetValue(LookAhead(0).Type, out parser);
            return parser != null ? parser.GetPrecedence() : 0;
        }

        protected void Register(TokenType token, IPrefixParselet<TExpression> parselet)
        {
            _prefixParselets[token] = parselet;
        }

        protected void Register(TokenType token, IInfixParselet<TExpression> parselet)
        {
            _infixParselets[token] = parselet;
        }

        /// <summary>
        /// Registers a postfix unary operator parselet for the given token and
        /// precedence.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="precedence"></param>
        protected void Postfix<TPostfixExpression>(TokenType token, int precedence)
            where TPostfixExpression : PostfixExpression<TExpression>, TExpression, new()
        {
            Register(token, new PostfixOperatorParselet<TExpression, TPostfixExpression>(precedence));
        }

        /// <summary>
        /// Registers a prefix unary operator parselet for the given token and
        /// precedence.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="precedence"></param>
        protected void Prefix<TPrefixExpression>(TokenType token, int precedence)
            where TPrefixExpression : PrefixExpression<TExpression>, TExpression, new()
        {
            Register(token, new PrefixOperatorParselet<TExpression, TPrefixExpression>(precedence));
        }

        /// <summary>
        /// Registers a left-associative binary operator parselet for the given token
        /// and precedence.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="precedence"></param>
        protected void InfixLeft<TOperatorExpression>(TokenType token, int precedence)
            where TOperatorExpression : OperatorExpression<TExpression>, TExpression, new()
        {
            Register(token, new BinaryOperatorParselet<TExpression, TOperatorExpression>(precedence, false));
        }

        /// <summary>
        /// Registers a right-associative binary operator parselet for the given token
        /// and precedence.
        /// </summary>
        /// <param name="token"></param>
        /// <param name="precedence"></param>
        protected void InfixRight<TOperatorExpression>(TokenType token, int precedence)
            where TOperatorExpression : OperatorExpression<TExpression>, TExpression, new()
        {
            Register(token, new BinaryOperatorParselet<TExpression, TOperatorExpression>(precedence, true));
        }
    }
}
