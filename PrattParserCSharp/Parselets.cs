﻿using System.Collections.Generic;

namespace General.PrattParser
{
    /// <summary>
    /// Parses assignment expressions like "a = b". The left side of an assignment
    /// expression must be a simple name like "a", and expressions are
    /// right-associative. (In other words, "a = b = c" is parsed as "a = (b = c)").
    /// </summary>
    public class AssignParselet<TExpression, TAssignExpression> : IInfixParselet<TExpression>
        where TExpression : IExpression
        where TAssignExpression : AssignExpression<TExpression>, TExpression, new()
    {
        public TExpression Parse(out string error, PrattParser<TExpression> parser, TExpression left, Token token)
        {
            var right = parser.ParseExpression(out error, (int)Precedence.Assignment - 1);
            if (!(left is NameExpression))
            {
                error = "The left-hand side of an assignment must be a name.";
                return default(TExpression);
            }
            var name = ((NameExpression)((IExpression)left)).Name;
            var asExp = new TAssignExpression();
            asExp.Assign(name, right);
            return asExp;
        }

        public int GetPrecedence()
        {
            return (int)Precedence.Assignment;
        }
    }

    /// <summary>
    /// Generic infix parselet for a binary arithmetic operator. The only
    /// difference when parsing, "+", "-", "*", "/", and "^" is precedence and
    /// associativity, so we can use a single parselet class for all of those.
    /// </summary>
    public class BinaryOperatorParselet<TExpression, TOperatorExpression> : IInfixParselet<TExpression>
        where TExpression : IExpression
        where TOperatorExpression : OperatorExpression<TExpression>, TExpression, new()
    {
        private readonly int _precedence;
        private readonly bool _isRight;

        public BinaryOperatorParselet(int precedence, bool isRight)
        {
            _precedence = precedence;
            _isRight = isRight;
        }

        public TExpression Parse(out string error, PrattParser<TExpression> parser, TExpression left, Token token)
        {
            // To handle right-associative operators like "^", we allow a slightly
            // lower precedence when parsing the right-hand side. This will let a
            // parselet with the same precedence appear on the right, which will then
            // take *this* parselet's result as its left-hand argument.
            var right = parser.ParseExpression(out error, _precedence - (_isRight ? 1 : 0));
            if (error != string.Empty) return default(TExpression);
            var opExp = new TOperatorExpression();
            opExp.Operate(left, token.Type, right);
            return opExp;
        }

        public int GetPrecedence()
        {
            return _precedence;
        }
    }

    /// <summary>
    /// Parselet to parse a function call like "a(b, c, d)".
    /// </summary>
    public class CallParselet<TExpression, TCallExpression> : IInfixParselet<TExpression>
        where TExpression : IExpression
        where TCallExpression : CallExpression<IExpression>, TExpression, new()
    {
        public TExpression Parse(out string error, PrattParser<TExpression> parser, TExpression left, Token token)
        {
            error = string.Empty;
            TCallExpression callExp;
            // Parse the comma-separated arguments until we hit, ")".
            var args = new List<IExpression>();
            // There may be no arguments at all.
            if (parser.Match(TokenType.RightParen))
            {
                callExp = new TCallExpression();
                callExp.Call(left, args);
                return callExp;
            }
            do
            {
                args.Add(parser.ParseExpression(out error));
                if (error != string.Empty) return default(TExpression);
            }
            while (parser.Match(TokenType.Comma));
            parser.Consume(out error, TokenType.RightParen);
            if (error != string.Empty) return default(TExpression);
            callExp = new TCallExpression();
            callExp.Call(left, args);
            return callExp;
        }

        public int GetPrecedence()
        {
            return (int)Precedence.Call;
        }
    }

    /// <summary>
    /// Parselet for the condition or "ternary" operator, like "a ? b : c".
    /// </summary>
    public class ConditionalParselet<TExpression, TConditionalExpression> : IInfixParselet<TExpression>
        where TExpression : IExpression
        where TConditionalExpression : ConditionalExpression<TExpression>, TExpression, new()
    {
        public TExpression Parse(out string error, PrattParser<TExpression> parser, TExpression left, Token token)
        {
            var thenArm = parser.ParseExpression(out error);
            parser.Consume(out error, TokenType.Colon);
            if (error != string.Empty) return default(TExpression);
            var elseArm = parser.ParseExpression(out error, (int)Precedence.Conditional - 1);
            if (error != string.Empty) return default(TExpression);
            var condExp = new TConditionalExpression();
            condExp.Conditional(left, thenArm, elseArm);
            return condExp;
        }

        public int GetPrecedence()
        {
            return (int)Precedence.Conditional;
        }
    }

    /// <summary>
    /// Parses parentheses used to group an expression, like "a * (b + c)".
    /// </summary>
    public class GroupParselet<TExpression> : IPrefixParselet<TExpression>
        where TExpression : IExpression
    {
        public TExpression Parse(out string error, PrattParser<TExpression> parser, Token token)
        {
            var expression = parser.ParseExpression(out error);
            parser.Consume(out error, TokenType.RightParen);
            return error != string.Empty ? default(TExpression) : expression;
        }
    }

    /// <summary>
    /// Simple parselet for a named variable like "abc".
    /// </summary>
    public class NameParselet<TExpression, TNameExpression> : IPrefixParselet<TExpression>
        where TExpression : IExpression
        where TNameExpression : NameExpression, TExpression, new()
    {
        public TExpression Parse(out string error, PrattParser<TExpression> parser, Token token)
        {
            error = string.Empty;
            var nameExp = new TNameExpression();
            nameExp.Named(token.Text);
            return nameExp;
        }
    }

    /// <summary>
    /// Generic infix parselet for an unary arithmetic operator. Parses postfix
    /// unary "?" expressions.
    /// </summary>
    public class PostfixOperatorParselet<TExpression, TPostfixExpression> : IInfixParselet<TExpression>
        where TExpression : IExpression
        where TPostfixExpression : PostfixExpression<TExpression>, TExpression, new()
    {
        private readonly int _precedence;

        public PostfixOperatorParselet(int precedence)
        {
            _precedence = precedence;
        }

        public TExpression Parse(out string error, PrattParser<TExpression> parser, TExpression left, Token token)
        {
            error = string.Empty;
            var postExp = new TPostfixExpression();
            postExp.Postfix(left, token.Type);
            return postExp;
        }

        public int GetPrecedence()
        {
            return _precedence;
        }
    }

    /// <summary>
    /// Generic prefix parselet for an unary arithmetic operator. Parses prefix
    /// unary "-", "+", "~", and "!" expressions.
    /// </summary>
    public class PrefixOperatorParselet<TExpression, TPrefixExpression> : IPrefixParselet<TExpression>
        where TExpression : IExpression
        where TPrefixExpression : PrefixExpression<TExpression>, TExpression, new()
    {
        private readonly int _precedence;

        public PrefixOperatorParselet(int precedence)
        {
            _precedence = precedence;
        }

        public TExpression Parse(out string error, PrattParser<TExpression> parser, Token token)
        {
            // To handle right-associative operators like "^", we allow a slightly
            // lower precedence when parsing the right-hand side. This will let a
            // parselet with the same precedence appear on the right, which will then
            // take *this* parselet's result as its left-hand argument.
            var right = parser.ParseExpression(out error, _precedence);
            if (error != string.Empty) return default(TExpression);
            var preExp = new TPrefixExpression();
            preExp.Prefix(token.Type, right);
            return preExp;
        }

        public int GetPrecedence()
        {
            return _precedence;
        }
    }

    public interface IInfixParselet<TExpression>
        where TExpression : IExpression
    {
        TExpression Parse(out string error, PrattParser<TExpression> parser, TExpression left, Token token);

        int GetPrecedence();
    }

    /// <summary>
    /// One of the two interfaces used by the Pratt parser. A PrefixParselet is
    /// associated with a token that appears at the beginning of an expression. Its
    /// parse() method will be called with the consumed leading token, and the
    /// parselet is responsible for parsing anything that comes after that token.
    /// This interface is also used for single-token expressions like variables, in
    /// which case parse() simply doesn't consume any more tokens.
    /// </summary>
    public interface IPrefixParselet<TExpression>
        where TExpression : IExpression
    {
        TExpression Parse(out string error, PrattParser<TExpression> parser, Token token);
    }
}
