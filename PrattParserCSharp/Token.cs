﻿namespace General.PrattParser
{
    public sealed class Token
    {
        public TokenType Type { get; private set; }
        public string Text { get; private set; }

        public Token(TokenType type, string text)
        {
            Type = type;
            Text = text;
        }
    }

    // Ordered in increasing precedence.
    public enum Precedence
    {
        Assignment = 1,
        Conditional = 2,
        Sum = 3,
        Product = 4,
        Exponent = 5,
        Prefix = 6,
        Postfix = 7,
        Call = 8
    }

    public enum TokenType
    {
        LeftParen,
        RightParen,
        And,
        Or,
        Comma,
        Assign,
        Plus,
        Minus,
        Asterisk,
        Slash,
        Caret,
        Tilde,
        Bang,
        Question,
        Colon,
        Name,
        Eof
    }

    public static class Helper
    {
        public static char Punctuator(this TokenType tokenType)
        {
            switch (tokenType)
            {
                case (TokenType.LeftParen): return '(';
                case (TokenType.RightParen): return ')';
                case (TokenType.And): return '&';
                case (TokenType.Or): return '|';
                case (TokenType.Comma): return ',';
                case (TokenType.Assign): return '=';
                case (TokenType.Plus): return '+';
                case (TokenType.Minus): return '-';
                case (TokenType.Asterisk): return '*';
                case (TokenType.Slash): return '/';
                case (TokenType.Caret): return '^';
                case (TokenType.Tilde): return '~';
                case (TokenType.Bang): return '!';
                case (TokenType.Question): return '?';
                case (TokenType.Colon): return ':';
                default: return default(char);
            }
        }
    }
}
