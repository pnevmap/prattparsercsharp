﻿using System.Collections.Generic;
using System.Text;

namespace General.PrattParser
{
    /// <summary>
    /// An assignment expression like "a = b".
    /// </summary>
    public class AssignExpression<TExpression> : IExpression
        where TExpression : IExpression
    {
        protected string Name;
        protected TExpression Right;

        public AssignExpression() { }

        public AssignExpression(string name, TExpression right)
        {
            Assign(name, right);
        }

        public void Assign(string name, TExpression right)
        {
            Name = name;
            Right = right;
        }

        public void Print(StringBuilder builder)
        {
            builder.Append("(").Append(Name).Append(" = ");
            Right.Print(builder);
            builder.Append(")");
        }
    }

    /// <summary>
    /// A function call like "a(b, c, d)".
    /// </summary>
    public class CallExpression<TExpression> : IExpression
        where TExpression : IExpression
    {
        protected TExpression Function;
        protected List<TExpression> Args;

        public CallExpression() { }

        public CallExpression(TExpression function, List<TExpression> args)
        {
            Call(function, args);
        }

        public void Call(TExpression function, List<TExpression> args)
        {
            Function = function;
            Args = args;
        }

        public void Print(StringBuilder builder)
        {
            Function.Print(builder);
            builder.Append("(");
            for (var i = 0; i < Args.Count; i++)
            {
                Args[i].Print(builder);
                if (i < Args.Count - 1) builder.Append(", ");
            }
            builder.Append(")");
        }
    }

    /// <summary>
    /// A ternary conditional expression like "a ? b : c".
    /// </summary>
    public class ConditionalExpression<TExpression> : IExpression
        where TExpression : IExpression
    {
        protected TExpression Condition;
        protected TExpression ThenArm;
        protected TExpression ElseArm;

        public ConditionalExpression() { }

        public ConditionalExpression(TExpression condition, TExpression thenArm, TExpression elseArm)
        {
            Conditional(condition, thenArm, elseArm);
        }

        public void Conditional(TExpression condition, TExpression thenArm, TExpression elseArm)
        {
            Condition = condition;
            ThenArm = thenArm;
            ElseArm = elseArm;
        }

        public void Print(StringBuilder builder)
        {
            builder.Append("(");
            Condition.Print(builder);
            builder.Append(" ? ");
            ThenArm.Print(builder);
            builder.Append(" : ");
            ElseArm.Print(builder);
            builder.Append(")");
        }
    }

    /// <summary>
    /// A simple variable name expression like "abc".
    /// </summary>
    public class NameExpression : IExpression
    {
        public string Name { get; private set; }

        public NameExpression() { }

        public NameExpression(string name)
        {
            Named(name);
        }

        public void Named(string name)
        {
            Name = name;
        }

        public void Print(StringBuilder builder)
        {
            builder.Append(Name);
        }
    }

    /// <summary>
    /// A binary arithmetic expression like "a + b" or "c ^ d".
    /// </summary>
    public class OperatorExpression<TExpression> : IExpression
        where TExpression : IExpression
    {
        protected TExpression Left;
        protected TokenType Operator;
        protected TExpression Right;

        public OperatorExpression() { }

        public OperatorExpression(TExpression left, TokenType bioperator, TExpression right)
        {
            Operate(left, bioperator, right);
        }

        public void Operate(TExpression left, TokenType bioperator, TExpression right)
        {
            Left = left;
            Operator = bioperator;
            Right = right;
        }

        public void Print(StringBuilder builder)
        {
            builder.Append("(");
            Left.Print(builder);
            builder.Append(" ").Append(Operator.Punctuator()).Append(" ");
            Right.Print(builder);
            builder.Append(")");
        }
    }

    /// <summary>
    /// A postfix unary arithmetic expression like "a!".
    /// </summary>
    public class PostfixExpression<TExpression> : IExpression
        where TExpression : IExpression
    {
        protected TExpression Left;
        protected TokenType Operator;

        public PostfixExpression() { }

        public PostfixExpression(TExpression left, TokenType postoperator)
        {
            Postfix(left, postoperator);
        }

        public void Postfix(TExpression left, TokenType postoperator)
        {
            Left = left;
            Operator = postoperator;
        }

        public void Print(StringBuilder builder)
        {
            builder.Append("(");
            Left.Print(builder);
            builder.Append(Operator.Punctuator()).Append(")");
        }
    }

    /// <summary>
    /// A prefix unary arithmetic expression like "!a" or "-b".
    /// </summary>
    public class PrefixExpression<TExpression> : IExpression
        where TExpression : IExpression
    {
        protected TokenType Operator;
        protected TExpression Right;

        public PrefixExpression() { }

        public PrefixExpression(TokenType preoperator, TExpression right)
        {
            Prefix(preoperator, right);
        }

        public void Prefix(TokenType preoperator, TExpression right)
        {
            Operator = preoperator;
            Right = right;
        }

        public void Print(StringBuilder builder)
        {
            builder.Append("(").Append(Operator.Punctuator());
            Right.Print(builder);
            builder.Append(")");
        }
    }

    /// <summary>
    /// Interface for all expression AST node classes.
    /// </summary>
    public interface IExpression
    {
        /// <summary>
        /// Pretty-print the expression to a string.
        /// </summary>
        /// <param name="builder">string builder</param>
        void Print(StringBuilder builder);
    }
}
